FROM ubuntu:18.04


RUN apt-get -y update && \
  apt-get install -y software-properties-common 

RUN apt-get install -y python3-pip \
    && apt-get clean \
    && python3 -m pip install --upgrade pip  


# update pip
RUN python3.6 -m pip install pip --upgrade
RUN python3.6 -m pip install wheel


RUN apt-get install -y --fix-missing \
    build-essential \
    cmake \
    gfortran \
    git \
    wget \
    curl \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libatlas-base-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    liblapack-dev \
    libswscale-dev \
    pkg-config \
    python3-dev \
    python3-numpy \
    software-properties-common \
    zip \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

COPY requirments.txt /app1/requirments.txt
WORKDIR /app1
RUN pip3 install -r requirments.txt
COPY . /app1



ENTRYPOINT [ "python3" ]

CMD [ "app1.py"]